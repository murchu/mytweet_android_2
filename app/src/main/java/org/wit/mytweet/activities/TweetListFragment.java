package org.wit.mytweet.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.settings.SettingsActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.wit.android.helpers.LogHelpers.info;

public class TweetListFragment extends ListFragment
    implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener
{

  /**
   * Adapter to render tweets in list view
   */
  private TweetAdapter adapter;

  MyTweetApp app;

  private TweetList tweetList;
  private ArrayList<Tweet> tweets;

  // ListView to assist with deleting tweets
  private ListView listView;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    getActivity().setTitle(R.string.app_name);

    app = MyTweetApp.getApp();
    tweetList = app.tweetList;
    tweets = app.tweetList.tweets;

    adapter = new TweetAdapter(getActivity(), tweets);
    setListAdapter(adapter);




    // callback to get all tweets
    Call<List<Tweet>> call = (Call<List<Tweet>>) app.tweetServiceOpen.getAllTweets();
    Toast.makeText(getContext(), "Checking server for new tweets", Toast.LENGTH_LONG).show();
    Log.v("MyTweet", "Checking server for new tweets");
    call.enqueue(new Callback<List<Tweet>>() {
      @Override
      public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
        Log.v("MyTweet", "Successfully retrieved tweets from server");
        if (response.body().size() > tweets.size())
        {
          tweetList.updateTweets((ArrayList<Tweet>) response.body());
          adapter = new TweetAdapter(getActivity(), tweets);
          setListAdapter(adapter);
          int newTweets = response.body().size() - tweets.size();
          Toast.makeText(getContext(), newTweets + " tweets updated from server", Toast.LENGTH_LONG).show();
          Log.v("MyTweet", "Updated local tweetlist & adapter with new network tweets");
        }
        else
        {
          Toast.makeText(getContext(), "No new tweets on sever", Toast.LENGTH_LONG).show();
        }
      }

      @Override
      public void onFailure(Call<List<Tweet>> call, Throwable t) {
        Toast toast = Toast.makeText(getContext(), "Unable to retrieve network tweets!", Toast.LENGTH_LONG);
        toast.show();
        Log.v("MyTweet", "Failed to retrieve network tweets!");
        info(this, t.toString());
      }
    });


  }

  @Override
  public void onPause()
  {
    super.onPause();
  }

  @Override
  public void onResume() {
    super.onResume();
    ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View view = super.onCreateView(inflater, parent, savedInstanceState);
    listView = (ListView) view.findViewById(android.R.id.list);
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listView.setMultiChoiceModeListener(this);
    return view;
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id)
  {
    Tweet twt = ((TweetAdapter) getListAdapter()).getItem(position);
    Intent i = new Intent(getActivity(), SendTweetPagerActivity.class);
    i.putExtra(SendTweetFragment.EXTRA_TWEET_ID, twt.getId());
    startActivityForResult(i, 0);
  }

  /**
   * Creates options menu
   *
   * @param menu
   * @return
   */
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.tweetlist, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      // new tweet button presses
      case R.id.menu_item_new_tweet:
        Intent i = new Intent(getActivity(), NewTweetActivity.class);
        this.startActivityForResult(i, 0);

        //Intent intent = new Intent(getActivity(), SendTweetPagerActivity.class);
        //this.startActivityForResult(intent, 0);
        return true;

      // refresh tweets option presses
      case R.id.action_refresh:
        retrieveTweets();
        return true;

      // clear tweets option presses
      case R.id.action_clear:
        app.tweetList.clearTweets();
        startActivity(new Intent(getActivity(), TweetListActivity.class));
        return true;

      // setting option presses
      case R.id.action_settings:
        startActivity(new Intent(getActivity(), SettingsActivity.class));
        return true;

      default: return super.onOptionsItemSelected(item);
    }
  }

  // retrieves tweets from api
  public void retrieveTweets()
  {
    Call<List<Tweet>> call = (Call<List<Tweet>>) app.tweetServiceOpen.getAllTweets();
    call.enqueue(new Callback<List<Tweet>>() {
      @Override
      public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
        Log.v("MyTweet", "Successfully retrieved tweets from server");
        if (response.body().size() > tweets.size())
        {
          tweetList.updateTweets((ArrayList<Tweet>) response.body());
          adapter = new TweetAdapter(getActivity(), tweets);
          setListAdapter(adapter);
          int newTweets = response.body().size() - tweets.size();
          Toast.makeText(getContext(), newTweets + " tweet(s) updated from server", Toast.LENGTH_LONG).show();
          Log.v("MyTweet", "Updated local tweetlist & adapter with new network tweets");
        }
        else
        {
          Toast.makeText(getContext(), "No new tweets on sever", Toast.LENGTH_LONG).show();
        }
      }

      @Override
      public void onFailure(Call<List<Tweet>> call, Throwable t) {
        Toast toast = Toast.makeText(getContext(), "Unable to retrieve network tweets!", Toast.LENGTH_LONG);
        toast.show();
        Log.v("MyTweet", "Failed to retrieve network tweets!");
        info(this, t.toString());
      }
    });
    Toast.makeText(getActivity(), "Retrieving tweet list", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Tweet tweet = adapter.getItem(position);
    IntentHelper.startActivityWithData(getActivity(), SendTweetPagerActivity.class, "EXTRA_TWEET_ID", tweet.getId());
  }

  // Multichoice mode listener method
  @Override
  public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
  {

  }

  // Multichoice mode listener method
  @Override
  public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
  {
    MenuInflater inflater = actionMode.getMenuInflater();
    inflater.inflate(R.menu.tweet_list_context, menu);
    return true;
  }

  // Multichoice mode listener method
  @Override
  public boolean onPrepareActionMode(ActionMode mode, Menu menu)
  {
    return false;
  }

  // Multichoice mode listener method
  @Override
  public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
  {
    switch (menuItem.getItemId())
    {
      case R.id.menu_item_delete_tweet:
        deleteTweet(actionMode);
        return true;
      default:
        return false;
    }
  }

  private void deleteTweet(ActionMode actionMode)
  {
    for (int i = adapter.getCount() - 1; i >= 0; i--)
    {
      if (listView.isItemChecked(i))
      {
        app.tweetList.deleteTweet(adapter.getItem(i));
      }
    }
    actionMode.finish();
    adapter.notifyDataSetChanged();
  }


  // Multichoice mode listener method
  @Override
  public void onDestroyActionMode(ActionMode mode)
  {

  }

}

/**
 * TweetAdapter class to adapt tweets for rendering on timeline
 */
class TweetAdapter extends ArrayAdapter<Tweet>
{
  private Context context;

  public TweetAdapter(Context context, ArrayList<Tweet> tweets)
  {
    super(context, 0, tweets);
    this.context = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if (convertView == null)
    {
      convertView = inflater.inflate(R.layout.list_item_tweet, null);
    }
    Tweet tweet = getItem(position);

    TextView contentTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_content);
    contentTextView.setText(tweet.getContent());

    TextView dateTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_date);
    dateTextView.setText(tweet.getDateString());

    return convertView;
  }

  public void updateAdapter(ArrayList<Tweet> tweets)
  {
    clear();
    addAll(tweets);
  }
}
