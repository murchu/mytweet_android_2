package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.R;

/**
 * Welcome class that governs the initial app welcome screen
 */
public class WelcomeActivity extends AppCompatActivity
    implements View.OnClickListener
{
  private Button signup;
  private Button login;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);

    // assign widgets
    signup = (Button) findViewById(R.id.welcomeSignupButton);
    login = (Button) findViewById(R.id.welcomeLoginButton);

    // assign listeners
    signup.setOnClickListener(this);
    login.setOnClickListener(this);

  }

  public void loginPressed(View view)
  {
    startActivity (new Intent(this, TweetListActivity.class));
  }

  public void signupPressed (View view)
  {
    startActivity (new Intent(this, SignupActivity.class));
  }

  /**
   * Listens for click events on activity screen
   *
   * @param view
   */
  @Override
  public void onClick(View view)
  {
    switch (view.getId())
    {
      // signup button presses
      case R.id.welcomeSignupButton:
        IntentHelper.startActivity(this, SignupActivity.class);
        break;

      // login button presses
      case R.id.welcomeLoginButton:
        IntentHelper.startActivity(this, LoginActivity.class);
        break;
    }
  }
}
