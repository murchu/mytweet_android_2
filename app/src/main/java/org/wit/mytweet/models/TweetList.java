package org.wit.mytweet.models;


import org.wit.mytweet.app.DbHelper;

import java.util.ArrayList;

import static org.wit.android.helpers.LogHelpers.info;

/**
 * TweetList class that represents a listeview of all tweets
 */
public class TweetList
{
  /**
   * Array that holds list of all tweets
   */
  public ArrayList<Tweet> tweets;

  private DbHelper dbHelper;

  public TweetList(DbHelper dbHelper)
  {
    this.dbHelper = dbHelper;
    this.tweets = (ArrayList<Tweet>) dbHelper.selectAllTweets();
  }

  // updates
  public void updateTweets(ArrayList<Tweet> listTweets)
  {
    tweets = listTweets;
    dbHelper.updateTweets(listTweets);
  }

  /**
   * Saves tweets to file
   */
  public boolean clearTweets()
  {
    try
    {
      tweets = new ArrayList<Tweet>();
      dbHelper.deleteAllTweets();
      info(this, "All tweets cleared");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error clearing tweets: " + e.getMessage());
      return false;
    }
  }

  /**
   * Returns the arraylist of tweets
   *
   * @return the arraylist of tweets
   */
  public ArrayList<Tweet> getTweets()
  {
    return tweets;
  }

  /**
   * Adds a tweet to the arraylist of tweets
   */
  public void addTweet(Tweet tweet)
  {
    tweets.add(tweet);
    dbHelper.addTweet(tweet);
  }

  /**
   * Udates a tweet in the tweetlist
   */
  public void updateTweet(Tweet tweet)
  {
    // find tweet in tweetlist
    for (Tweet twt : tweets)
    {
      if (twt.getId().equals(tweet.getId()))
      {
        //twt.setContent(tweet.getContent());
        //saveTweets();
        twt = tweet;
        dbHelper.updateTweet(tweet);
        info(this, "Updated Tweet content!");
      }
    }
  }

  /**
   * Deletes a tweet from the arraylist of tweets
   */
  public void deleteTweet(Tweet tweet)
  {
    tweets.remove(tweet);
    dbHelper.deleteTweet(tweet);
  }

  /**
   * Generates sample tweets and populates TweetList with these tweets
   */
  private void generateSampleTweets()
  {
    for (int i = 0; i < 50; i++)
    {
      addTweet(new Tweet("Test tweet " + i));
    }
  }

  /**
   * Returns the tweet that corresponds to the id submitted
   *
   * @param id id of tweet to search for
   * @return the tweet that matches the id submitted
   */
  public Tweet getTweet(Long id)
  {
    for (Tweet tweet : tweets)
    {
      if (id.equals(tweet.getId()))
      {
        return tweet;
      }
    }
    return null;
  }

  /**
   * Returns the tweet that corresponds to the UUID submitted
   */
  public Tweet getTweet(String _id)
  {
    for (Tweet tweet : tweets)
    {
      if (_id.equals(tweet.getId()))
      {
        return tweet;
      }
    }
    return null;
  }

  public boolean containsTweet(Tweet tweet)
  {
    return tweets.contains(tweet);
  }

}
