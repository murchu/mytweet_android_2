package org.wit.mytweet.app;

import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface TweetServiceOpen
{
  @POST("/api/users/authenticate")
  Call<Token> authenticateUser(@Body User user);

  @GET("/api/users/{id}")
  Call<User> getUser(@Path("id") String id);

  @POST("/api/users")
  Call<User> registerUser(@Body User user);

  @GET("/api/tweets/{id}")
  Call<Tweet> getTweet(@Path("id") String id);

  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();

  @GET("/api/users/{id}/tweets")
  Call<List<Tweet>> getTweetsByUser(@Path("id") String id);
}
